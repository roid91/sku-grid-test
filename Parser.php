<?php

namespace SkuGrid;

/**
 * Базовый класс парсера на curl
 *
 * язык запросов может быть расширяемым (см. interface Query )
 * по-умолчанию это XPath
 *
 * P.S. объявил несколько классов и интерфейс в одном (данном) файле, чтобы не "раздувать" визуально тестовое задание
 *
 * @package SkuGrid
 */
class Parser
{
    /** @var string ссылка на страницу для парсинга */
    protected $url = '';

    /** @var string кодировка документа */
    public $encoding = 'utf-8';

    /** @var int таймаут запроса в сек. */
    public $timeout = 10;

    /** @var int max кол-во редиректов curl */
    public $max_redirs = 3;

    /** @var string UserAgent - браузер */
    public $useragent = 'Wget/1.16 (linux-gnu)'; // с моего сервера заработало с таким юзерагентом
//    public $useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.104 Safari/537.36';

    /** @var int|null HTTP-код ответа curl */
    protected $last_code = null;

    /** @var string|array cookie */
    public $cookies = [];

    protected $html;
    protected $query;

    protected static $query_instance = [];

    const QTYPE_XPATH = 'XPath';

    public function __construct()
    {
    }

    /**
     * Выполнить curl-запрос
     *
     * @param string|null $method метод запроса (GET, POST и тд), GET по-умолчанию
     * @param string|null $fileds набор полей для запроса, например из $_POST
     * @return string|null
     */
    protected function getContent($method = 'GET', $fileds = null)
    {

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Accept-Language: ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
            'Connection: keep-alive'
        ]);

        curl_setopt($ch, CURLOPT_USERAGENT, $this->useragent);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT, $this->timeout);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_AUTOREFERER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_MAXREDIRS, $this->max_redirs);

        // если POST-запрос
        if (isset($method) && strtolower($method) == 'post' && isset($fileds)) {
            curl_setopt($ch, CURLOPT_REFERER, $this->url);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $fileds);
        }

        curl_setopt($ch, CURLOPT_ENCODING, '');

        // получаем куки
        curl_setopt($ch, CURLOPT_HEADERFUNCTION, function ($ch, $headers) {
            preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $headers, $matches);

            foreach ($matches[1] as $item) {
                $item = trim($item);
                if (strlen($item)) {
                    if (!is_array($this->cookies)) {
                        $this->cookies = explode('; ', $this->cookies);
                        if (!$this->cookies[0])
                            $this->cookies = [];
                    }
                    $this->cookies[] = $item;
                }
            }

            return strlen($headers);
        });

        // устанавливаем куки
        if (is_array($this->cookies))
            $this->cookies = implode('; ', $this->cookies);
        if (isset($this->cookies) && is_string($this->cookies) && strlen($this->cookies))
            curl_setopt($ch, CURLOPT_COOKIE, $this->cookies);

        // curl_setopt($ch, CURLOPT_VERBOSE, true);

        $output = curl_exec($ch);

        $this->last_code = (curl_getinfo($ch, CURLINFO_HTTP_CODE));

        curl_close($ch);

        return $output;
    }

    /**
     * Получить последний HTTP-код ответа curl
     *
     * @return int|null
     */
    public function getLastCode()
    {
        return $this->last_code;
    }

    /**
     * Выполнить запрос XPath и др.
     *
     * @param string $expression запрос
     * @param string $query_type язык запроса
     * @return mixed
     */
    public function query($expression, $query_type = self::QTYPE_XPATH)
    {
        if ($query_type == self::QTYPE_XPATH) {
            if (!isset(self::$query_instance[$query_type]))
                self::$query_instance[$query_type] = new XPath($this->html, $this->encoding);
        } else {
            // сюда можно добавить определения других способов извлечения данных (phpQuery и др.)
        }

        return self::$query_instance[$query_type]->query($expression);
    }

    /**
     * Выполнить запрос по ссылке, учитывая $method и $fields
     *
     * @param string $url ссылка
     * @param string $method метод GET, POST и тд
     * @param array $fields поля для POST и др.
     * @return null|string
     */
    public function request($url, $method = 'GET', $fields = [])
    {
        self::$query_instance = [];

        $this->url = $url;
        $this->html = $this->getContent($method, $fields);

        if (isset($this->last_code) && $this->last_code != 200)
            die('Error loading ' . $this->url . ', code - ' . $this->last_code);

        return $this->html;
    }

}

/**
 * Interface Query
 *
 * Интерфейс для определения языков запросов
 *
 * @package SkuGrid
 */
interface Query
{
    /**
     * Выполняем запрос
     *
     * @param string $expression
     * @return mixed
     */
    public function query($expression);

    /**
     * Получить результат запроса в виде HTML
     *
     * @return mixed
     */
    public function html();

    /**
     * Получить результат запроса в тестовом виде
     * @return string
     */
    public function text();

    /**
     * Получить значение аттрибута HTML
     *
     * @param string $name имя аттрибута
     * @return string
     */
    public function attr($name);

    /**
     * Применить замыкание к результату запроса
     *
     * @param \Closure $func замыкание
     * @return string
     */
    public function func(\Closure $func);

    /**
     * Получить результат запроса
     *
     * @return string
     */
    public function get();
}

/**
 * Class XPath реализующий интерфейс Query
 *
 * @package SkuGrid
 */
class XPath implements Query
{

    protected $xpath;
    protected $dom;

    /** @var array ноды XPath */
    protected $nodes = [];

    /** @var string результат запроса */
    protected $content = '';

    /**
     * XPath constructor.
     * @param string $html
     * @param string|null $encoding
     */
    public function __construct(&$html, $encoding = null)
    {
        // для кириллицы
        if ($encoding)
            $html = mb_convert_encoding($html, 'html-entities', $encoding);

        $this->loadDOM($html);
        $this->loadXPath();
    }

    public function __toString()
    {
        return $this->content;
    }

    /**
     * Выполняем запрос XPath
     *
     * @param string $expression XPath-выражение
     * @return $this
     */
    public function query($expression)
    {
        if (count($this->nodes)) {
            $this->nodes = $this->walkNodes(function ($node) use ($expression) {
                return $this->toAttay($this->xpath->query($expression, $node));
            });
        } else {
            $this->nodes = $this->toAttay($this->xpath->query($expression));
        }

        return $this;
    }

    public function html()
    {
        $this->content = $this->walkNodes(function ($node) {
            if (isset($node->ownerDocument))
                return $node->ownerDocument->saveHTML($node);
            else
                return null;
        });

        return $this;
    }

    public function text()
    {
        $this->content = $this->walkNodes(function ($node) {
            return $node->textContent;
        });

        return $this;
    }

    public function attr($name)
    {
        $this->content = $this->walkNodes(function ($node) use ($name) {
            return $node->getAttribute($name);
        });

        return $this;
    }

    public function func(\Closure $func)
    {
        $this->content = $func($this->content);

        return $this;
    }

    public function get()
    {
        return $this->content;
    }


    /**
     * Преобразовать ноды XPath в массив
     *
     * @param \DOMNodeList $nodes
     * @return array
     */
    protected function toAttay(\DOMNodeList $nodes)
    {
        $result = [];

        foreach ($nodes as $v)
            $result[] = $v;

        return $result;
    }

    /**
     * Применить замыкание к каждой ноде XPath
     *
     * @param \Closure $func
     * @param array $nodes
     * @return array|mixed|null
     */
    protected function walkNodes(\Closure $func, &$nodes = [])
    {
        $result = [];

        if (!isset($nodes) || !count($nodes))
            $nodes = &$this->nodes;

        if (isset($func) && is_callable($func)) {
            if (isset($nodes) && count($nodes)) {
                foreach ($nodes as $k => $node) {
                    if (is_array($node) && count($node))
                        $result[$k] = $this->walkNodes($func, $node);
                    else
                        $result[] = $func($node);
                }
            }
        }

        if (count($result))
            return count($nodes) == 1 ? $result[0] : $result;
        else
            return null;
    }

    protected function loadDOM(&$html)
    {
        $this->dom = new \DOMDocument();

        // включение обработки ошибок пользователем
        libxml_use_internal_errors(true);
        $this->dom->loadHTML($html);
        libxml_clear_errors();

    }

    protected function loadXPath()
    {
        $this->xpath = new \DOMXPath($this->dom);
    }

}