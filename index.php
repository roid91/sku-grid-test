<?php
/*
Задан товар:
http://www.walmart.com/ip/Danskin-Now-Women-s-Knit-Slip-on-Shoe/51630300

Задача:
1) Найти все варианты данного товара (цвет-размер).
       а.Найти наличие на складе каждого варианта/
       b. Найти цену для каждого варианта/
2) Найти все варианты (название, стоимость) доставки данного товара по индексу 10001.
3) Код должен иметь возможность работать с другими товарами данного магазина.

Код должен быть написан на PHP с комментариями, можно использовать любые библиотеки.
 */

require_once('Parser.php');


// почтовый индес
$index = 10001;

// адрес загружаемой страницы
$url = 'http://www.walmart.com/ip/Danskin-Now-Women-s-Knit-Slip-on-Shoe/51630300';
if (isset($_POST['url']))
    $url = $_POST['url'];

// адрес страницы расчета доставки
$url_shipping = 'https://www.walmart.com/terra-firma/item/#PRODUCT_ID#/location/#INDEX#?selected=true&wl13=';


//////////////////////////////////////////////////////////

$parser = new SkuGrid\Parser();

// кодировка страницы (не обязательно)
$parser->encoding = 'utf-8';

// таймаут подключения, сек. ($this->request)
$parser->timeout = 3;

// см. класс Parser, чтобы узнать все параметры

// массив результатов
$result = [];

/*******************************************************/


if (parse_url($url)['host'] != 'www.walmart.com')
    die('BAD url');

// загружаем страницу в парсер
$parser->request($url);

// получаем размеры и цвета
$result['sizes_colors'] = [];

$product = $parser
    ->query('//head/script[contains(text(), "window.__WML_REDUX_INITIAL_STATE__ =")]')
    ->text()
    ->func(function ($data) {
        $matches = [];
        preg_match('/window\.__WML_REDUX_INITIAL_STATE__ = (\{.+?\});/s', $data, $matches);

        return json_decode($matches[1], true)['product'];
    })
    ->get();

foreach ($product['products'] as $v) {
    $result['sizes_colors'][$v['variants']['size']]['name'] = $product['variantCategoriesMap'][$product['primaryProduct']]['size']['variants'][$v['variants']['size']]['name'];
    $result['sizes_colors'][$v['variants']['size']]['colors'][] = [
        'name' => $product['variantCategoriesMap'][$product['primaryProduct']]['actual_color']['variants'][$v['variants']['actual_color']]['name'],
        'status' => $product['offers'][$v['offers'][0]]['productAvailability']['availabilityStatus'],
        'price' => $product['offers'][$v['offers'][0]]['pricesInfo']['priceMap']['CURRENT']
    ];
}
ksort($result['sizes_colors'], SORT_NATURAL);
///

// получаем данные о доставке
$result['shipping_data'] = [];

$url_shipping = str_replace(['#PRODUCT_ID#', '#INDEX#'], [$product['primaryProduct'], $index], $url_shipping);
$data = $parser->request($url_shipping);

$shipping_data = json_decode($data, true)['payload']['offers'];
foreach ($shipping_data as $k => $v) {
    if (isset($v['upsellFulfillmentOption'])) {
        $result['shipping_data'][] = [
            'upsellFulfillmentOption' => $v['upsellFulfillmentOption'],
            'fulfillment' => $v['fulfillment'],
        ];
    }
}
///

/*******************************************************/

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Parser</title>
</head>
<body>

<?php
/*echo '<pre>';
print_r($result);
echo '</pre>';*/
?>

<form method="post">
    <input size="80" type="text" name="url" value="<?= htmlspecialchars($url) ?>"/>
    <input type="submit" name="send" value="GO!"/>
</form>

<?php if (count($result['sizes_colors'])) { ?>
    <?php foreach ($result['sizes_colors'] as $v) { ?>
        <h3>Size: <?= $v['name'] ?></h3>
        <?php foreach ($v['colors'] as $v2) { ?>
            <div>
                <?= $v2['name'] ?>
                <small>(<?= $v2['price']['currencyUnitSymbol'] . $v2['price']['price'] ?>
                    , <?= ($v2['status'] == 'IN_STOCK' ? '<font color="#0c0">' . $v2['status'] . '</font>' : '<font color="#c00">' . $v2['status'] . '</font>') ?>
                    )
                </small>
            </div>
        <?php } ?>
        <br>
    <?php } ?>
<?php } ?>
<br>

<h3>Shippment</h3>
<?php if (count($result['shipping_data'])) { ?>
    <?php foreach ($result['shipping_data'] as $v) { ?>
        <?php if ($v['fulfillment']['pickupable']) { ?>
            <?php foreach ($v['fulfillment']['pickupOptions'] as $v2) { ?>
                <div>Pickup: <?= $v['upsellFulfillmentOption']['displayArrivalDate'] ?>
                    (<?= $v2['storeName'] . ', ' . $v2['storeAddress'] ?>
                    , <?= $v2['availability'] . ', ' . $v2['pickupMethod'] ?>)
                </div>
            <?php } ?>
        <?php } ?>
        <?php if ($v['fulfillment']['shippable']) { ?>
            <div>Delivery: <?= $v['upsellFulfillmentOption']['displayArrivalDate'] ?>
                (<?= $v['upsellFulfillmentOption']['shippingPrice']['currencyUnitSymbol'] . $v['upsellFulfillmentOption']['shippingPrice']['price'] ?>
                , <?= $v['upsellFulfillmentOption']['shipMethod'] ?>)
            </div>
        <?php } ?>
    <?php } ?>
<?php } else { ?>
    <div>- not available -</div>
<?php } ?>

</body>
</html>